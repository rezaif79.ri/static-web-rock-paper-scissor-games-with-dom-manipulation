const element = document.querySelectorAll("#player > a");
const element_comp = document.querySelectorAll("#computer > a");
const versus = document.getElementById("vs");
const refresh = document.getElementById("refresh-btn"); 
const playerCounter = document.getElementById('player-counter');
const compCounter = document.getElementById('computer-counter');

const gamesVal = ['batu', 'kertas', 'gunting'];
const clap = new Audio('https://actions.google.com/sounds/v1/crowds/battle_crowd_celebrate_stutter.ogg');

const pilih = [];

function addGamesClickListener(){

	element.forEach(p => {
		p.onclick = function clicked(){
			clearSelection();
			element.forEach(p => {p.onclick = function(){return false}}); // Disable onclick 
			
			p.classList.add('games-active');
			pilih.push(gamesVal.findIndex(games => games == p.id));
			console.log('Player pick ', p.id);

			const comp = randomComp();
			checkGames(pilih[0], comp); // run checkGames -> checkWinner -> change result element
			refresh.onclick = refreshGames;
			refresh.style.visibility = "visible";			
		}
	});

	
}

function refreshGames(){
	clearSelection();
	addGamesClickListener();
	
	versus.textContent = "VS";
	refresh.style.visibility = "hidden";
}



function clearSelection(){
	if(pilih.length){
		pilih.pop();
	}
	element.forEach(p => {
		p.classList.remove('games-active');
	});
	element_comp.forEach(c =>{
		c.classList.remove('games-active');
	});
	versus.classList.remove('games-result');
	versus.classList.remove('games-draw');
}

function randomComp(){
	const random = Math.floor(Math.random() * 3);
	console.log('Computer pick ', gamesVal[random]);

	element_comp[random].classList.add('games-active');

	return random;
}

function checkGames(pilih, comp){
	const rounds = [pilih, comp]
	const check = Math.abs(rounds[0] - rounds[1]);
	let hasil;
	switch(check){
		case 0:
			hasil = -1;
			versus.classList.add('games-draw');
			versus.innerHTML = "DRAW";
			console.log('Games result is DRAW');
			break;
		case 1:
			hasil = Math.max.apply(Math, rounds);
			checkWinner(pilih, hasil);
			break;
		case 2:
			hasil = Math.min.apply(Math, rounds);
			checkWinner(pilih, hasil);
			break;
	}
}

function checkWinner(pilih, hasil){
	versus.classList.add('games-result');
	if(hasil == pilih){
		console.log('Player WINS');
		versus.innerHTML = "PLAYER 1 </br> WINS";
		playerCounter.innerHTML++;

		clap.play();
		setTimeout(() => {
			clap.pause();
			clap.currentTime = 0;
		}, 3000);
	}else{
		console.log('Computer WINS');
		versus.innerHTML = "COMP </br> WINS";
		compCounter.innerHTML++;
	}
}


// On-Load-Complete
window.addEventListener('load', function(){
	addGamesClickListener();
}, false );